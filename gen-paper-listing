#!/usr/bin/env php
<?php
/* Copyright 2018 Romain "Artefact2" Dal Maso <artefact2@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

if($argc !== 2) {
	fprintf(STDERR, "Usage: %s <out.pdf> < <input-file>\n", $argv[0]);
	die(1);
}

register_shutdown_function(function() {
	passthru('find . -maxdepth 1 -name ".'.getmypid().'-*.*" -delete');
});

const PAPER_W = 210;
const PAPER_H = 297;
const PAPER_MARGIN = 5;
const FONT = "monospace";
const FONT_SIZE = 8 / 72.0 * 25.4;
const CHUNK_SIZE = 24;
const HASH_SIZE = 8;

$nlines = (int)((PAPER_H - 2.0 * PAPER_MARGIN) / FONT_SIZE);
$npage = 0;
$linelen = 4 * CHUNK_SIZE;
$offset = 0;
$hctx = hash_init('sha3-512');

while(($chunk = fread(STDIN, ($nlines - 4) * 3 * CHUNK_SIZE)) !== false && $chunk !== '') {
	$f = fopen($fn = sprintf('.%d-%08d.svg', getmypid(), $npage++), 'w');
	fwrite($f, '<?xml version="1.0" encoding="utf-8" standalone="no" ?>');
	fprintf($f, '<svg width="%fmm" height="%fmm" viewBox="0 0 %f %f" xmlns="http://www.w3.org/2000/svg">', PAPER_W, PAPER_H, PAPER_W, PAPER_H);
	fprintf($f, '<g transform="translate(%f %f)"><text font-family="%s" font-size="%f" style="font-weight: bold; white-space: pre;">', PAPER_MARGIN, PAPER_MARGIN, FONT, FONT_SIZE);

	fprintf($f, '<tspan x="0" dy="1em">%s | %s | %s</tspan>', str_pad('OFFSET', 8), str_pad('DATA', $linelen), str_pad('SHA3-512', HASH_SIZE));
	fprintf($f, '<tspan x="0" dy="1em">%s-|-%s-|-%s</tspan>', str_repeat('-', 8), str_repeat('-', $linelen), str_repeat('-', HASH_SIZE));

	while($chunk !== '' && $chunk !== false) {
		$l = substr($chunk, 0, 3 * CHUNK_SIZE);
		hash_update($hctx, $l);
		$l = base64_encode($l);

		fprintf(
			$f, '<tspan x="0" dy="1em">%08x | %s | %s</tspan>',
			$offset,
			str_pad($l, $linelen),
			substr(base64_encode(hash('sha3-512', $l, true)), 0, HASH_SIZE)
			);

		$chunk = substr($chunk, 3 * CHUNK_SIZE);
		$offset += 3 * CHUNK_SIZE;
	}

	fprintf($f, '<tspan x="0" dy="2em">SHA3-512(DATA)=%s</tspan>', substr(base64_encode(hash_final(hash_copy($hctx), true)), 0, HASH_SIZE + $linelen - 1));
	fwrite($f, '</text></g></svg>');
	fclose($f);
	echo $fn, PHP_EOL;

	passthru(sprintf('inkscape --export-filename=%s.pdf %s', $fn, $fn));
}

passthru('pdfunite .'.getmypid().'-*.pdf '.escapeshellarg($argv[1]));
